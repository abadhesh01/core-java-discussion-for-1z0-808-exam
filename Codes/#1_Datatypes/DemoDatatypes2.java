
// Author: Abadhesh Mishra [Employee Id: 8117322]

public class DemoDatatypes2 {
    public static void main(String[] args) {
        System.out.println();

        /* 
           Note: 
           -----
           [1] Binary representation of decimal number =>
               -> Only '0b' or '0B' prefix reprsents binary number.
               -> In binary representation after prefix 
                  only combination of 0 and 1 are allowed.

           [2] Hexadecimal representation of decimal number =>
               -> Only '0x' or '0X' prefix reprsents hexadecimal number.
               -> In hexadecimal representation after prefix 
                  only combination of A to F (uppercase characters), 
                  a to f (lowercase characters) and 0 to 9 are allowed.   
                  
           [3] Octal representation of decimal number =>
               -> Only '0' prefix reprsents octal number.
               -> In octal representation after prefix 
                  only combination of 0 to 7 are allowed.      
        */

        // Binary number.
        int number = 0b1101100; // Decimal number: 108
        System.out.println("number: " + number + ", Binary Format: " + Integer.toBinaryString(number));

        // Binary signed 2's complement.
        number = 0B0000000101101000; // Decimal number: 360 
        System.out.println("\nnumber : " + number + ", Binary Format: " + Integer.toBinaryString(number));

        // Hexadecimal number.
        number = 0x179A9; // Decimal number: 96681   
        System.out.println("\nnumber : " + number + ", Hexadecimal Format: " + Integer.toHexString(number));

        // Hexadecimal signed 2's complement.
        number = 0X000095d3; // Decimal number: 38355
        System.out.println("\nnumber : " + number + ", Hexadecimal Format: " + Integer.toHexString(number));

        // Octal number.
        number = 01303; // Decimal number: 707
        System.out.println("\nnumber : " + number + ", Octal Format: " + Integer.toOctalString(number));

        // Some other representation of 'double' type value. 
        double sampleDoubleNumber = 7700000.12345;
        System.out.println("\nsampleDoubleNumber: " + sampleDoubleNumber);

        // 'E' represents the number of digits left to the decimal point.
        double sampleDoubleNumber2 = 77.0000012345E5; // Same as '7700000.12345'.
        System.out.println("sampleDoubleNumber2: " + sampleDoubleNumber2);

        // 'e' also represents the number of digits left to the decimal point.
        double sampleDoubleNumber3 = 77.0000012345e5; // Same as '7700000.12345'.
        System.out.println("sampleDoubleNumber3: " + sampleDoubleNumber3);

        double sampleDoubleNumber4 = 77.0000012345e0005; // Same as '7700000.12345'.
        System.out.println("sampleDoubleNumber4: " + sampleDoubleNumber4);

        // Checking equality of 'double' type variables.....
        System.out.println("[sampleDoubleNumber == sampleDoubleNumber2]: " 
        + (sampleDoubleNumber == sampleDoubleNumber2));

        System.out.println("[sampleDoubleNumber == sampleDoubleNumber3]: " 
        + (sampleDoubleNumber == sampleDoubleNumber3));

        System.out.println("[sampleDoubleNumber == sampleDoubleNumber4]: " 
        + (sampleDoubleNumber == sampleDoubleNumber4));

        // A sample hexadecimal floating point value of type 'double'.
        // Here 'p' indicates hexadecimal floating point number.
        double sampleHexadecimalFloatingPointNumber = 0x1.91eb851eb851fp1; // Output: 3.14
        System.out.println("\nsampleHexadecimalFloatingPointNumber: " 
        + sampleHexadecimalFloatingPointNumber); 

        // Anathor sample hexadecimal floating point value of type 'double'.
        // Here 'P' indicates hexadecimal floating point number.
        double sampleHexadecimalFloatingPointNumber2 = 0X1.91EB851EB851FP1;
        System.out.println("sampleHexadecimalFloatingPointNumber2: " // Output: 3.14
        + sampleHexadecimalFloatingPointNumber2);

        // Checking equality of the above floating point values.....
        System.out.println("[sampleHexadecimalFloatingPointNumber == sampleHexadecimalFloatingPointNumber2]: "
        + (sampleHexadecimalFloatingPointNumber == sampleHexadecimalFloatingPointNumber2));

        /* Compilation Error: 
           ---------------------
           * Reason:
             '0' is the prefix so, the number is an octal number. 
              In octal number, after prefix digits from 0 to 7 are allowed only.

            * Error type: "The literal 08 of type int is out of range" 

            * This rule is also applied for all other representations.
        */ 
        //number = 08; // Uncomment and run to see the output and comment again for successful compilation.

        System.out.println();
    }
}
